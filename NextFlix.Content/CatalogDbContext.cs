using Microsoft.EntityFrameworkCore;
using NextFlix.Content.Model;

namespace NextFlix.Content;

public class CatalogDbContext : DbContext
{
    public DbSet<Movie> Movies { get; set; }
    public DbSet<Genre> Genres { get; set; }
    public DbSet<Person> Persons { get; set; }

    public CatalogDbContext(DbContextOptions<CatalogDbContext> options) : base(options)
    {
    }
    
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Movie>()
            .HasOne(m => m.Director)
            .WithMany(d => d.Movies)
            .HasForeignKey(m => m.DirectorId);

        modelBuilder.Entity<Movie>()
            .HasOne(m => m.Genre)
            .WithMany(g => g.Movies)
            .HasForeignKey(m => m.GenreId);

        foreach (var genre in _genres)
        {
            modelBuilder.Entity<Genre>().HasData(genre);
        }
        
        foreach (var person in _persons)
        {
            modelBuilder.Entity<Person>().HasData(person);
        }
        
        foreach (var movie in _movies)
        {
            movie.IsActive = true;
            modelBuilder.Entity<Movie>().HasData(movie);
        }
    }

    private static readonly List<Genre> _genres = new()
    {
        new Genre { Id = Guid.NewGuid(), Name = "Action" },
        new Genre { Id = Guid.NewGuid(), Name = "Science Fiction" },
        new Genre { Id = Guid.NewGuid(), Name = "Drama" },
        new Genre { Id = Guid.NewGuid(), Name = "Thriller" },
        new Genre { Id = Guid.NewGuid(), Name = "Fantasy" },
        new Genre { Id = Guid.NewGuid(), Name = "Adventure" },
        new Genre { Id = Guid.NewGuid(), Name = "Crime" },
        new Genre { Id = Guid.NewGuid(), Name = "Biography" },
        new Genre { Id = Guid.NewGuid(), Name = "Mystery" },
        new Genre { Id = Guid.NewGuid(), Name = "Romance" },
    };

    private static readonly List<Person> _persons = new()
    {
        new Person { Id = Guid.NewGuid(), FirstName = "Christopher", LastName = "Nolan" },
        new Person { Id = Guid.NewGuid(), FirstName = "Lana", LastName = "Wachowski" },
        new Person { Id = Guid.NewGuid(), FirstName = "Lilly", LastName = "Wachowski" },
        new Person { Id = Guid.NewGuid(), FirstName = "Bong", LastName = "Joon-ho" },
        new Person { Id = Guid.NewGuid(), FirstName = "Denis", LastName = "Villeneuve" },
        new Person { Id = Guid.NewGuid(), FirstName = "Anthony", LastName = "Russo" },
        new Person { Id = Guid.NewGuid(), FirstName = "Joe", LastName = "Russo" },
        new Person { Id = Guid.NewGuid(), FirstName = "Barry", LastName = "Jenkins" },
        new Person { Id = Guid.NewGuid(), FirstName = "Alfonso", LastName = "Cuarón" },
        new Person { Id = Guid.NewGuid(), FirstName = "George", LastName = "Miller" },
        new Person { Id = Guid.NewGuid(), FirstName = "Todd", LastName = "Phillips" },
        new Person { Id = Guid.NewGuid(), FirstName = "Wes", LastName = "Anderson" },
        new Person { Id = Guid.NewGuid(), FirstName = "Spike", LastName = "Jonze" },
        new Person { Id = Guid.NewGuid(), FirstName = "Joel", LastName = "Coen" },
        new Person { Id = Guid.NewGuid(), FirstName = "Ethan", LastName = "Coen" },
        new Person { Id = Guid.NewGuid(), FirstName = "Martin", LastName = "Scorsese" },
        new Person { Id = Guid.NewGuid(), FirstName = "David", LastName = "Fincher" },
        new Person { Id = Guid.NewGuid(), FirstName = "Quentin", LastName = "Tarantino" },
    };

    private static readonly List<Movie> _movies = new()
    { 
        new Movie { Id = Guid.NewGuid(), Title = "Inception", EnglishTitle = "Inception", Origin = "USA", ReleasedAt = DateTime.SpecifyKind(new DateTime(2010, 7, 16), DateTimeKind.Utc), Cover = "https://example.com/inception.jpg", Trailer = "", Description = "A thief who steals corporate secrets through the use of dream-sharing technology is given the inverse task of planting an idea into the mind of a CEO.", DirectorId = GetRandomDirectorId(), GenreId = GetRandomGenreId(), Price = 399 },
        new Movie { Id = Guid.NewGuid(), Title = "The Matrix", EnglishTitle = "The Matrix", Origin = "USA", ReleasedAt = DateTime.SpecifyKind(new DateTime(1999, 3, 31), DateTimeKind.Utc), Cover = "https://example.com/matrix.jpg", Trailer = "", Description = "When a beautiful stranger leads computer hacker Neo to a forbidding underworld, he discovers the shocking truth--the life he knows is the elaborate deception of an evil cyber-intelligence.", DirectorId = GetRandomDirectorId(), GenreId = GetRandomGenreId(), Price = 299 },
        new Movie { Id = Guid.NewGuid(), Title = "Interstellar", EnglishTitle = "Interstellar", Origin = "USA", ReleasedAt = DateTime.SpecifyKind(new DateTime(2014, 11, 7), DateTimeKind.Utc), Cover = "https://example.com/interstellar.jpg", Trailer = "", Description = "A team of explorers travel through a wormhole in space in an attempt to ensure humanity's survival.", DirectorId = GetRandomDirectorId(), GenreId = GetRandomGenreId(), Price = 499 },
        new Movie { Id = Guid.NewGuid(), Title = "Parasite", EnglishTitle = "Parasite", Origin = "South Korea", ReleasedAt = DateTime.SpecifyKind(new DateTime(2019, 5, 30), DateTimeKind.Utc), Cover = "https://example.com/parasite.jpg", Trailer = "", Description = "Greed and class discrimination threaten the newly formed symbiotic relationship between the wealthy Park family and the destitute Kim clan.", DirectorId = GetRandomDirectorId(), GenreId = GetRandomGenreId(), Price = 350 },
        new Movie { Id = Guid.NewGuid(), Title = "Blade Runner 2049", EnglishTitle = "Blade Runner 2049", Origin = "USA", ReleasedAt = DateTime.SpecifyKind(new DateTime(2017, 10, 6), DateTimeKind.Utc), Cover = "https://example.com/bladerunner2049.jpg", Trailer = "", Description = "Young Blade Runner K's discovery of a long-buried secret leads him to track down former Blade Runner Rick Deckard, who's been missing for thirty years.", DirectorId = GetRandomDirectorId(), GenreId = GetRandomGenreId(), Price = 399 },
        new Movie { Id = Guid.NewGuid(), Title = "Avengers: Endgame", EnglishTitle = "Avengers: Endgame", Origin = "USA", ReleasedAt = DateTime.SpecifyKind(new DateTime(2019, 4, 26), DateTimeKind.Utc), Cover = "https://example.com/avengersendgame.jpg", Trailer = "", Description = "The Avengers assemble once more in order to reverse Thanos' actions and restore balance to the universe.", DirectorId = GetRandomDirectorId(), GenreId = GetRandomGenreId(), Price = 499 },
        new Movie { Id = Guid.NewGuid(), Title = "Moonlight", EnglishTitle = "Moonlight", Origin = "USA", ReleasedAt = DateTime.SpecifyKind(new DateTime(2016, 10, 21), DateTimeKind.Utc), Cover = "https://example.com/moonlight.jpg", Trailer = "", Description = "A young African-American man grapples with his identity and sexuality while experiencing the everyday struggles of childhood, adolescence, and burgeoning adulthood.", DirectorId = GetRandomDirectorId(), GenreId = GetRandomGenreId(), Price = 299 },
        new Movie { Id = Guid.NewGuid(), Title = "Gravity", EnglishTitle = "Gravity", Origin = "USA", ReleasedAt = DateTime.SpecifyKind(new DateTime(2013, 10, 4), DateTimeKind.Utc), Cover = "https://example.com/gravity.jpg", Trailer = "", Description = "Two astronauts work together to survive after an accident leaves them stranded in space.", DirectorId = GetRandomDirectorId(), GenreId = GetRandomGenreId(), Price = 399 },
        new Movie { Id = Guid.NewGuid(), Title = "Mad Max: Fury Road", EnglishTitle = "Mad Max: Fury Road", Origin = "Australia", ReleasedAt = DateTime.SpecifyKind(new DateTime(2015, 5, 15), DateTimeKind.Utc), Cover = "https://example.com/madmax.jpg", Trailer = "", Description = "In a post-apocalyptic wasteland, a woman rebels against a tyrannical ruler in search for her homeland with the aid of a group of female prisoners, a psychotic worshiper, and a drifter named Max.", DirectorId = GetRandomDirectorId(), GenreId = GetRandomGenreId(), Price = 399 },
        new Movie { Id = Guid.NewGuid(), Title = "Joker", EnglishTitle = "Joker", Origin = "USA", ReleasedAt = DateTime.SpecifyKind(new DateTime(2019, 10, 4), DateTimeKind.Utc), Cover = "https://example.com/joker.jpg", Trailer = "", Description = "In Gotham City, mentally troubled comedian Arthur Fleck is disregarded and mistreated by society. He then embarks on a downward spiral of revolution and bloody crime. This path brings him face-to-face with his alter-ego: the Joker.", DirectorId = GetRandomDirectorId(), GenreId = GetRandomGenreId(), Price = 499 },
        new Movie { Id = Guid.NewGuid(), Title = "The Grand Budapest Hotel", EnglishTitle = "The Grand Budapest Hotel", Origin = "USA", ReleasedAt = DateTime.SpecifyKind(new DateTime(2014, 3, 28), DateTimeKind.Utc), Cover = "https://example.com/grandbudapest.jpg", Trailer = "", Description = "A writer encounters the owner of an aging high-class hotel, who tells him of his early years serving as a lobby boy in the hotel's glorious years under an exceptional concierge.", DirectorId = GetRandomDirectorId(), GenreId = GetRandomGenreId(), Price = 299 },
        new Movie { Id = Guid.NewGuid(), Title = "Her", EnglishTitle = "Her", Origin = "USA", ReleasedAt = DateTime.SpecifyKind(new DateTime(2013, 12, 18), DateTimeKind.Utc), Cover = "https://example.com/her.jpg", Trailer = "", Description = "In a near future, a lonely writer develops an unlikely relationship with an operating system designed to meet his every need.", DirectorId = GetRandomDirectorId(), GenreId = GetRandomGenreId(), Price = 350 },
        new Movie { Id = Guid.NewGuid(), Title = "No Country for Old Men", EnglishTitle = "No Country for Old Men", Origin = "USA", ReleasedAt = DateTime.SpecifyKind(new DateTime(2007, 11, 21), DateTimeKind.Utc), Cover = "https://example.com/nocountry.jpg", Trailer = "", Description = "Violence and mayhem ensue after a hunter stumbles upon a drug deal gone wrong and more than two million dollars in cash near the Rio Grande.", DirectorId = GetRandomDirectorId(), GenreId = GetRandomGenreId(), Price = 299 },
        new Movie { Id = Guid.NewGuid(), Title = "Shutter Island", EnglishTitle = "Shutter Island", Origin = "USA", ReleasedAt = DateTime.SpecifyKind(new DateTime(2010, 2, 19), DateTimeKind.Utc), Cover = "https://example.com/shutterisland.jpg", Trailer = "", Description = "In 1954, a U.S. Marshal investigates the disappearance of a murderer who escaped from a hospital for the criminally insane.", DirectorId = GetRandomDirectorId(), GenreId = GetRandomGenreId(), Price = 399 },
        new Movie { Id = Guid.NewGuid(), Title = "The Revenant", EnglishTitle = "The Revenant", Origin = "USA", ReleasedAt = DateTime.SpecifyKind(new DateTime(2016, 1, 8), DateTimeKind.Utc), Cover = "https://example.com/revenant.jpg", Trailer = "", Description = "A frontiersman on a fur trading expedition in the 1820s fights for survival after being mauled by a bear and left for dead by members of his own hunting team.", DirectorId = GetRandomDirectorId(), GenreId = GetRandomGenreId(), Price = 450 },
        new Movie { Id = Guid.NewGuid(), Title = "Whiplash", EnglishTitle = "Whiplash", Origin = "USA", ReleasedAt = DateTime.SpecifyKind(new DateTime(2014, 10, 15), DateTimeKind.Utc), Cover = "https://example.com/whiplash.jpg", Trailer = "", Description = "A promising young drummer enrolls at a cut-throat music conservatory where his dreams of greatness are mentored by an instructor who will stop at nothing to realize a student's potential.", DirectorId = GetRandomDirectorId(), GenreId = GetRandomGenreId(), Price = 299 },
        new Movie { Id = Guid.NewGuid(), Title = "La La Land", EnglishTitle = "La La Land", Origin = "USA", ReleasedAt = DateTime.SpecifyKind(new DateTime(2016, 12, 9), DateTimeKind.Utc), Cover = "https://example.com/lalaland.jpg", Trailer = "", Description = "While navigating their careers in Los Angeles, a pianist and an actress fall in love while attempting to reconcile their aspirations for the future.", DirectorId = GetRandomDirectorId(), GenreId = GetRandomGenreId(), Price = 399 },
        new Movie { Id = Guid.NewGuid(), Title = "Frozen", EnglishTitle = "Frozen", Origin = "USA", ReleasedAt = DateTime.SpecifyKind(new DateTime(2013, 11, 27), DateTimeKind.Utc), Cover = "https://example.com/frozen.jpg", Trailer = "", Description = "When the newly crowned Queen Elsa accidentally uses her power to turn things into ice to curse her home in infinite winter, her sister, Anna, teams up with a mountain man, his playful reindeer, and a snowman to change the weather condition.", DirectorId = GetRandomDirectorId(), GenreId = GetRandomGenreId(), Price = 350 },
        new Movie { Id = Guid.NewGuid(), Title = "The Social Network", EnglishTitle = "The Social Network", Origin = "USA", ReleasedAt = DateTime.SpecifyKind(new DateTime(2010, 10, 1), DateTimeKind.Utc), Cover = "https://example.com/socialnetwork.jpg", Trailer = "", Description = "As Harvard student Mark Zuckerberg creates the social networking site that would become known as Facebook, he is sued by the twins who claimed he stole their idea, and by the co-founder who was later squeezed out of the business.", DirectorId = GetRandomDirectorId(), GenreId = GetRandomGenreId(), Price = 399 },
        new Movie { Id = Guid.NewGuid(), Title = "Pulp Fiction", EnglishTitle = "Pulp Fiction", Origin = "USA", ReleasedAt = DateTime.SpecifyKind(new DateTime(1994, 10, 14), DateTimeKind.Utc), Cover = "https://example.com/pulpfiction.jpg", Trailer = "", Description = "The lives of two mob hitmen, a boxer, a gangster's wife, and a pair of diner bandits intertwine in four tales of violence and redemption.", DirectorId = GetRandomDirectorId(), GenreId = GetRandomGenreId(), Price = 499 }
    };
    
    private static Guid GetRandomGenreId()
        => _genres.OrderBy(g => Guid.NewGuid()).First().Id;

    private static Guid GetRandomDirectorId()
        => _persons.OrderBy(d => Guid.NewGuid()).First().Id;
}
using System.ComponentModel.DataAnnotations;

namespace NextFlix.Content.Model;

public class Movie
{
    [Key]
    public Guid Id { get; set; }
    
    [Required]
    public string Title { get; set; }
    
    [Required]
    public string EnglishTitle { get; set; }
    
    public string Origin { get; set; }
    
    public DateTime ReleasedAt { get; set; }
    
    public string Cover { get; set; }
    
    public string Trailer { get; set; }
    public string Description { get; set; }
    
    public Guid DirectorId { get; set; }
    public Person Director { get; set; }
    
    public Guid GenreId { get; set; }
    public Genre Genre { get; set; }
    
    public int Price { get; set; }
    
    public bool IsActive { get; set; }
}
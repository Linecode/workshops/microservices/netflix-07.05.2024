using System.ComponentModel.DataAnnotations;

namespace NextFlix.Content.Model;

public class Genre
{
    [Key]
    public Guid Id { get; set; }
    
    [Required]
    public string Name { get; set; }

    public ICollection<Movie> Movies { get; set; } = new List<Movie>();
}
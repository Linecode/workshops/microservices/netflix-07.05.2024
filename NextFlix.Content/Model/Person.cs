using System.ComponentModel.DataAnnotations;

namespace NextFlix.Content.Model;

public class Person
{
    [Key]
    public Guid Id { get; set; }
    
    [Required]
    public string FirstName { get; set; }
    
    [Required]
    public string LastName { get; set; }

    public ICollection<Movie> Movies { get; set; } = new List<Movie>();
}
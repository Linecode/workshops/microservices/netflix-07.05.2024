﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace NextFlix.Content.Migrations
{
    /// <inheritdoc />
    public partial class InitialMigration : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Genres",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Genres", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Persons",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    FirstName = table.Column<string>(type: "text", nullable: false),
                    LastName = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Persons", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Movies",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Title = table.Column<string>(type: "text", nullable: false),
                    EnglishTitle = table.Column<string>(type: "text", nullable: false),
                    Origin = table.Column<string>(type: "text", nullable: false),
                    ReleasedAt = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Cover = table.Column<string>(type: "text", nullable: false),
                    Trailer = table.Column<string>(type: "text", nullable: false),
                    Description = table.Column<string>(type: "text", nullable: false),
                    DirectorId = table.Column<Guid>(type: "uuid", nullable: false),
                    GenreId = table.Column<Guid>(type: "uuid", nullable: false),
                    Price = table.Column<int>(type: "integer", nullable: false),
                    IsActive = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Movies_Genres_GenreId",
                        column: x => x.GenreId,
                        principalTable: "Genres",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Movies_Persons_DirectorId",
                        column: x => x.DirectorId,
                        principalTable: "Persons",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Genres",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { new Guid("0e2f52ec-2efd-4904-84aa-29cf06f974fc"), "Thriller" },
                    { new Guid("21336be5-358c-4623-9b2c-e192bbae4e53"), "Mystery" },
                    { new Guid("255174d5-c0ed-4ea0-bac7-b1f81b67c4e9"), "Fantasy" },
                    { new Guid("2fe09b02-9c05-4048-bfbb-713af5771541"), "Romance" },
                    { new Guid("35ac9def-e871-46b7-b75c-0a5ea21d5469"), "Biography" },
                    { new Guid("8e40c2cd-ff6c-4b77-b26a-fa874a9bc7a5"), "Adventure" },
                    { new Guid("ad3081e3-486c-4a29-a2e3-d5a8ad4efacc"), "Science Fiction" },
                    { new Guid("c8cd39df-b1c6-4e1e-a770-02baad3afd3a"), "Drama" },
                    { new Guid("ded13f50-4206-409a-8537-a841927a3919"), "Action" },
                    { new Guid("ffb5efc6-98d8-42f6-bf55-6a855c9b866b"), "Crime" }
                });

            migrationBuilder.InsertData(
                table: "Persons",
                columns: new[] { "Id", "FirstName", "LastName" },
                values: new object[,]
                {
                    { new Guid("0bac67b9-c0cd-4f9c-95a2-e33f77e97840"), "David", "Fincher" },
                    { new Guid("1a2ce7a8-c895-4704-85bf-6d316a456ed8"), "George", "Miller" },
                    { new Guid("314252ae-5334-4e59-8332-d3a87b927ac1"), "Alfonso", "Cuarón" },
                    { new Guid("3350355b-204f-4093-8849-17e9c841da20"), "Joe", "Russo" },
                    { new Guid("5b96d968-95e4-4e76-9b6b-0f6baf9d915b"), "Joel", "Coen" },
                    { new Guid("71d1e801-e274-4f3a-b7b7-b708b3d02f88"), "Denis", "Villeneuve" },
                    { new Guid("7abba7a3-5c26-4f47-8e70-b56057dc1375"), "Wes", "Anderson" },
                    { new Guid("9402635a-36d5-4961-ba87-2b335d5919d9"), "Barry", "Jenkins" },
                    { new Guid("a57d44f6-b1fe-4177-9594-5d3ef5592444"), "Todd", "Phillips" },
                    { new Guid("ab7921e6-f2c9-423b-b132-426ca537e801"), "Lilly", "Wachowski" },
                    { new Guid("ac09e7c4-30ba-4f6a-9fa0-148a7611a607"), "Lana", "Wachowski" },
                    { new Guid("ad3272be-ce1e-4e16-9b5f-1ea13e84fdd5"), "Ethan", "Coen" },
                    { new Guid("af5b9d80-2608-46ac-8469-b3a1d08cefc2"), "Spike", "Jonze" },
                    { new Guid("bea49ee1-a124-4186-8353-cdf9f6bc220e"), "Christopher", "Nolan" },
                    { new Guid("c07e1e93-f627-4f3b-bc95-c36098bf23ab"), "Anthony", "Russo" },
                    { new Guid("c8a1b0f8-fbeb-400e-ad74-eba5c12e1891"), "Martin", "Scorsese" },
                    { new Guid("d5943a17-61df-4ce4-ac02-7bed4c2def4e"), "Quentin", "Tarantino" },
                    { new Guid("dcdbf003-c102-464f-8231-b0bd3d3a3570"), "Bong", "Joon-ho" }
                });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "Cover", "Description", "DirectorId", "EnglishTitle", "GenreId", "IsActive", "Origin", "Price", "ReleasedAt", "Title", "Trailer" },
                values: new object[,]
                {
                    { new Guid("017e6c58-27c2-41ba-a2a8-b1221cd5fbbe"), "https://example.com/avengersendgame.jpg", "The Avengers assemble once more in order to reverse Thanos' actions and restore balance to the universe.", new Guid("c07e1e93-f627-4f3b-bc95-c36098bf23ab"), "Avengers: Endgame", new Guid("ffb5efc6-98d8-42f6-bf55-6a855c9b866b"), true, "USA", 499, new DateTime(2019, 4, 26, 0, 0, 0, 0, DateTimeKind.Utc), "Avengers: Endgame", "" },
                    { new Guid("03a283c4-9a61-47da-92f3-9c0619f5645f"), "https://example.com/nocountry.jpg", "Violence and mayhem ensue after a hunter stumbles upon a drug deal gone wrong and more than two million dollars in cash near the Rio Grande.", new Guid("af5b9d80-2608-46ac-8469-b3a1d08cefc2"), "No Country for Old Men", new Guid("0e2f52ec-2efd-4904-84aa-29cf06f974fc"), true, "USA", 299, new DateTime(2007, 11, 21, 0, 0, 0, 0, DateTimeKind.Utc), "No Country for Old Men", "" },
                    { new Guid("0d340d4e-6117-4d58-9d62-a6eb99264133"), "https://example.com/parasite.jpg", "Greed and class discrimination threaten the newly formed symbiotic relationship between the wealthy Park family and the destitute Kim clan.", new Guid("314252ae-5334-4e59-8332-d3a87b927ac1"), "Parasite", new Guid("35ac9def-e871-46b7-b75c-0a5ea21d5469"), true, "South Korea", 350, new DateTime(2019, 5, 30, 0, 0, 0, 0, DateTimeKind.Utc), "Parasite", "" },
                    { new Guid("4284dcfa-617a-4101-919b-721766fb2615"), "https://example.com/joker.jpg", "In Gotham City, mentally troubled comedian Arthur Fleck is disregarded and mistreated by society. He then embarks on a downward spiral of revolution and bloody crime. This path brings him face-to-face with his alter-ego: the Joker.", new Guid("314252ae-5334-4e59-8332-d3a87b927ac1"), "Joker", new Guid("35ac9def-e871-46b7-b75c-0a5ea21d5469"), true, "USA", 499, new DateTime(2019, 10, 4, 0, 0, 0, 0, DateTimeKind.Utc), "Joker", "" },
                    { new Guid("42e62df1-dc6e-488b-ae79-8cb9a2637e0f"), "https://example.com/socialnetwork.jpg", "As Harvard student Mark Zuckerberg creates the social networking site that would become known as Facebook, he is sued by the twins who claimed he stole their idea, and by the co-founder who was later squeezed out of the business.", new Guid("0bac67b9-c0cd-4f9c-95a2-e33f77e97840"), "The Social Network", new Guid("ad3081e3-486c-4a29-a2e3-d5a8ad4efacc"), true, "USA", 399, new DateTime(2010, 10, 1, 0, 0, 0, 0, DateTimeKind.Utc), "The Social Network", "" },
                    { new Guid("4c246fc5-ab2b-41f8-9f1b-1ce290e28825"), "https://example.com/moonlight.jpg", "A young African-American man grapples with his identity and sexuality while experiencing the everyday struggles of childhood, adolescence, and burgeoning adulthood.", new Guid("9402635a-36d5-4961-ba87-2b335d5919d9"), "Moonlight", new Guid("ffb5efc6-98d8-42f6-bf55-6a855c9b866b"), true, "USA", 299, new DateTime(2016, 10, 21, 0, 0, 0, 0, DateTimeKind.Utc), "Moonlight", "" },
                    { new Guid("53aec932-bd96-48ee-a973-0bf21de35900"), "https://example.com/interstellar.jpg", "A team of explorers travel through a wormhole in space in an attempt to ensure humanity's survival.", new Guid("9402635a-36d5-4961-ba87-2b335d5919d9"), "Interstellar", new Guid("8e40c2cd-ff6c-4b77-b26a-fa874a9bc7a5"), true, "USA", 499, new DateTime(2014, 11, 7, 0, 0, 0, 0, DateTimeKind.Utc), "Interstellar", "" },
                    { new Guid("6b3bb770-ed3b-44ee-a3c4-b327b3cc0e4c"), "https://example.com/matrix.jpg", "When a beautiful stranger leads computer hacker Neo to a forbidding underworld, he discovers the shocking truth--the life he knows is the elaborate deception of an evil cyber-intelligence.", new Guid("a57d44f6-b1fe-4177-9594-5d3ef5592444"), "The Matrix", new Guid("255174d5-c0ed-4ea0-bac7-b1f81b67c4e9"), true, "USA", 299, new DateTime(1999, 3, 31, 0, 0, 0, 0, DateTimeKind.Utc), "The Matrix", "" },
                    { new Guid("6e7c73ff-40b2-4862-a301-c04f39ce8c4d"), "https://example.com/her.jpg", "In a near future, a lonely writer develops an unlikely relationship with an operating system designed to meet his every need.", new Guid("af5b9d80-2608-46ac-8469-b3a1d08cefc2"), "Her", new Guid("2fe09b02-9c05-4048-bfbb-713af5771541"), true, "USA", 350, new DateTime(2013, 12, 18, 0, 0, 0, 0, DateTimeKind.Utc), "Her", "" },
                    { new Guid("8e6407eb-dc70-4f21-8b30-517d43ff59cd"), "https://example.com/pulpfiction.jpg", "The lives of two mob hitmen, a boxer, a gangster's wife, and a pair of diner bandits intertwine in four tales of violence and redemption.", new Guid("ad3272be-ce1e-4e16-9b5f-1ea13e84fdd5"), "Pulp Fiction", new Guid("255174d5-c0ed-4ea0-bac7-b1f81b67c4e9"), true, "USA", 499, new DateTime(1994, 10, 14, 0, 0, 0, 0, DateTimeKind.Utc), "Pulp Fiction", "" },
                    { new Guid("a29eeae7-54e7-4096-9685-cd5409f7de8a"), "https://example.com/madmax.jpg", "In a post-apocalyptic wasteland, a woman rebels against a tyrannical ruler in search for her homeland with the aid of a group of female prisoners, a psychotic worshiper, and a drifter named Max.", new Guid("71d1e801-e274-4f3a-b7b7-b708b3d02f88"), "Mad Max: Fury Road", new Guid("255174d5-c0ed-4ea0-bac7-b1f81b67c4e9"), true, "Australia", 399, new DateTime(2015, 5, 15, 0, 0, 0, 0, DateTimeKind.Utc), "Mad Max: Fury Road", "" },
                    { new Guid("a749822e-b2c6-4f8c-9917-6734e78ea601"), "https://example.com/bladerunner2049.jpg", "Young Blade Runner K's discovery of a long-buried secret leads him to track down former Blade Runner Rick Deckard, who's been missing for thirty years.", new Guid("bea49ee1-a124-4186-8353-cdf9f6bc220e"), "Blade Runner 2049", new Guid("ad3081e3-486c-4a29-a2e3-d5a8ad4efacc"), true, "USA", 399, new DateTime(2017, 10, 6, 0, 0, 0, 0, DateTimeKind.Utc), "Blade Runner 2049", "" },
                    { new Guid("a7a1fdea-f85f-49f6-9c24-1d1f27d55f7d"), "https://example.com/shutterisland.jpg", "In 1954, a U.S. Marshal investigates the disappearance of a murderer who escaped from a hospital for the criminally insane.", new Guid("9402635a-36d5-4961-ba87-2b335d5919d9"), "Shutter Island", new Guid("c8cd39df-b1c6-4e1e-a770-02baad3afd3a"), true, "USA", 399, new DateTime(2010, 2, 19, 0, 0, 0, 0, DateTimeKind.Utc), "Shutter Island", "" },
                    { new Guid("aaadfb5c-46fe-401b-8a60-18b14c73839f"), "https://example.com/revenant.jpg", "A frontiersman on a fur trading expedition in the 1820s fights for survival after being mauled by a bear and left for dead by members of his own hunting team.", new Guid("c8a1b0f8-fbeb-400e-ad74-eba5c12e1891"), "The Revenant", new Guid("8e40c2cd-ff6c-4b77-b26a-fa874a9bc7a5"), true, "USA", 450, new DateTime(2016, 1, 8, 0, 0, 0, 0, DateTimeKind.Utc), "The Revenant", "" },
                    { new Guid("c6c2aeb6-ef17-4d94-83d7-876b9b00fb3d"), "https://example.com/frozen.jpg", "When the newly crowned Queen Elsa accidentally uses her power to turn things into ice to curse her home in infinite winter, her sister, Anna, teams up with a mountain man, his playful reindeer, and a snowman to change the weather condition.", new Guid("a57d44f6-b1fe-4177-9594-5d3ef5592444"), "Frozen", new Guid("c8cd39df-b1c6-4e1e-a770-02baad3afd3a"), true, "USA", 350, new DateTime(2013, 11, 27, 0, 0, 0, 0, DateTimeKind.Utc), "Frozen", "" },
                    { new Guid("cfa17401-9c6e-4c42-a2a7-661f5b162113"), "https://example.com/lalaland.jpg", "While navigating their careers in Los Angeles, a pianist and an actress fall in love while attempting to reconcile their aspirations for the future.", new Guid("3350355b-204f-4093-8849-17e9c841da20"), "La La Land", new Guid("c8cd39df-b1c6-4e1e-a770-02baad3afd3a"), true, "USA", 399, new DateTime(2016, 12, 9, 0, 0, 0, 0, DateTimeKind.Utc), "La La Land", "" },
                    { new Guid("cff55d95-f40a-428e-ba94-92a62d0608c5"), "https://example.com/grandbudapest.jpg", "A writer encounters the owner of an aging high-class hotel, who tells him of his early years serving as a lobby boy in the hotel's glorious years under an exceptional concierge.", new Guid("ab7921e6-f2c9-423b-b132-426ca537e801"), "The Grand Budapest Hotel", new Guid("35ac9def-e871-46b7-b75c-0a5ea21d5469"), true, "USA", 299, new DateTime(2014, 3, 28, 0, 0, 0, 0, DateTimeKind.Utc), "The Grand Budapest Hotel", "" },
                    { new Guid("dd608a10-f0a4-469f-82ce-b74b385f71be"), "https://example.com/gravity.jpg", "Two astronauts work together to survive after an accident leaves them stranded in space.", new Guid("ab7921e6-f2c9-423b-b132-426ca537e801"), "Gravity", new Guid("c8cd39df-b1c6-4e1e-a770-02baad3afd3a"), true, "USA", 399, new DateTime(2013, 10, 4, 0, 0, 0, 0, DateTimeKind.Utc), "Gravity", "" },
                    { new Guid("fbae62e5-ab80-451f-a309-fd7fbc7ca5ad"), "https://example.com/inception.jpg", "A thief who steals corporate secrets through the use of dream-sharing technology is given the inverse task of planting an idea into the mind of a CEO.", new Guid("ab7921e6-f2c9-423b-b132-426ca537e801"), "Inception", new Guid("255174d5-c0ed-4ea0-bac7-b1f81b67c4e9"), true, "USA", 399, new DateTime(2010, 7, 16, 0, 0, 0, 0, DateTimeKind.Utc), "Inception", "" },
                    { new Guid("fc70e471-228f-4d7c-8741-458bc03ee5ca"), "https://example.com/whiplash.jpg", "A promising young drummer enrolls at a cut-throat music conservatory where his dreams of greatness are mentored by an instructor who will stop at nothing to realize a student's potential.", new Guid("af5b9d80-2608-46ac-8469-b3a1d08cefc2"), "Whiplash", new Guid("255174d5-c0ed-4ea0-bac7-b1f81b67c4e9"), true, "USA", 299, new DateTime(2014, 10, 15, 0, 0, 0, 0, DateTimeKind.Utc), "Whiplash", "" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Movies_DirectorId",
                table: "Movies",
                column: "DirectorId");

            migrationBuilder.CreateIndex(
                name: "IX_Movies_GenreId",
                table: "Movies",
                column: "GenreId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Movies");

            migrationBuilder.DropTable(
                name: "Genres");

            migrationBuilder.DropTable(
                name: "Persons");
        }
    }
}

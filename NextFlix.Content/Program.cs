using System.Text.Json.Serialization;
using Microsoft.EntityFrameworkCore;
using NextFlix.Content;
using NextFlix.Content.Model;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.ConfigureHttpJsonOptions(opt =>
{
    opt.SerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles;
});

builder.Services.AddDbContext<CatalogDbContext>(options =>
    options.UseNpgsql(builder.Configuration.GetConnectionString("DefaultConnection")));

var app = builder.Build();

using var scope = app.Services.CreateScope();
await using var dbContext = scope.ServiceProvider.GetRequiredService<CatalogDbContext>();
await dbContext.Database.MigrateAsync();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

// AuthN / AuthZ - 
// Testy - 
// Observability

app.UseHttpsRedirection();

app.MapGet("/api/movie/{id}", async (Guid id, CatalogDbContext context) =>
{
    var movie = await context.Movies
        .Include(x => x.Genre)
        .Include(x => x.Director)
        .FirstOrDefaultAsync(m => m.Id == id);
    
    return movie == null ? Results.NotFound() : Results.Ok(movie);
}).WithOpenApi();

app.MapGet("/api/movies", async (CatalogDbContext context) =>
{
    var movies = await context.Movies
       .Include(x => x.Genre)
       .Include(x => x.Director)
       .ToListAsync();
    
    return Results.Ok(movies);
})
.WithOpenApi();

app.MapPost("/api/movie", async (CreateMovie request, CatalogDbContext context) =>
{
    var movie = new Movie
    {
        Id = Guid.NewGuid(),
        Title = request.Title,
        EnglishTitle = request.EnglishTitle,
        Origin = request.Origin,
        ReleasedAt = request.ReleasedAt,
        Cover = request.Cover,
        Trailer = request.Trailer,
        Description = request.Description,
        DirectorId = request.DirectorId,
        GenreId = request.GenreId,
        Price = request.Price,
        IsActive = true
    };
    context.Movies.Add(movie);
    await context.SaveChangesAsync();
    
    return Results.Created($"/api/movie/{movie.Id}", movie);
}).WithOpenApi();

app.MapPut("/api/movie/{id}", async (Guid id, CreateMovie request, CatalogDbContext context) =>
{
    var movie = await context.Movies.FirstOrDefaultAsync(m => m.Id == id);
    if (movie == null)
        return Results.NotFound();
    
    movie.Title = request.Title;
    movie.EnglishTitle = request.EnglishTitle;
    movie.Origin = request.Origin;
    movie.ReleasedAt = request.ReleasedAt;
    movie.Cover = request.Cover;
    movie.Trailer = request.Trailer;
    movie.Description = request.Description;
    movie.DirectorId = request.DirectorId;
    movie.GenreId = request.GenreId;
    movie.Price = request.Price;
    
    await context.SaveChangesAsync();
    
    return Results.Ok(movie);
}).WithOpenApi();

app.MapDelete("/api/movie/{id}", async (Guid id, CatalogDbContext context) =>
{
    var movie = await context.Movies.FirstOrDefaultAsync(m => m.Id == id);
    if (movie == null)
        return Results.NotFound();
    
    context.Movies.Remove(movie);
    await context.SaveChangesAsync();
    
    return Results.NoContent();
});

app.MapGet("/api/genres", async (CatalogDbContext context) =>
{
    var genres = await context.Genres.ToListAsync();
    
    return Results.Ok(genres);
}).WithOpenApi();

app.MapGet("/api/genre/{id}", async (Guid id, CatalogDbContext context) =>
{
    var genre = await context.Genres.Include(x => x.Movies).FirstOrDefaultAsync(g => g.Id == id);
    
    return genre == null? Results.NotFound() : Results.Ok(genre);
}).WithOpenApi();

app.MapPost("/api/genre", async (CreateGenre request, CatalogDbContext context) =>
{
    var genre = new Genre
    {
        Id = Guid.NewGuid(),
        Name = request.Name
    };
    context.Genres.Add(genre);
    await context.SaveChangesAsync();
    
    return Results.Created($"/api/genre/{genre.Id}", genre);
}).WithOpenApi();

app.MapDelete("/api/genre/{id}", async (Guid id, CatalogDbContext context) =>
{
    var genre = await context.Genres.FirstOrDefaultAsync(g => g.Id == id);
    if (genre == null)
        return Results.NotFound();
    
    context.Genres.Remove(genre);
    await context.SaveChangesAsync();
    
    return Results.NoContent();
}).WithOpenApi();

app.MapPut("/api/genre/{id}", async (Guid id, CreateGenre request, CatalogDbContext context) =>
{
    var genre = await context.Genres.FirstOrDefaultAsync(g => g.Id == id);
    if (genre == null)
        return Results.NotFound();
    
    genre.Name = request.Name;
    
    await context.SaveChangesAsync();
    
    return Results.Ok(genre);
}).WithOpenApi();

app.MapGet("/api/directors", async (CatalogDbContext context) =>
{
    var directors = await context.Persons.ToListAsync();
    
    return Results.Ok(directors);
}).WithOpenApi();

app.MapGet("/api/director/{id}", async (Guid id, CatalogDbContext context) =>
{
    var director = await context.Persons.Include(x => x.Movies).FirstOrDefaultAsync(d => d.Id == id);
    
    return director == null? Results.NotFound() : Results.Ok(director);
}).WithOpenApi();

app.MapPost("/api/director", async (CreateDirector request, CatalogDbContext context) =>
{
    var director = new Person
    {
        Id = Guid.NewGuid(),
        FirstName = request.FirstName,
        LastName = request.LastName
    };
    context.Persons.Add(director);
    await context.SaveChangesAsync();
    
    return Results.Created($"/api/director/{director.Id}", director);
});

app.MapDelete("/api/director/{id}", async (Guid id, CatalogDbContext context) =>
{
    var director = await context.Persons.FirstOrDefaultAsync(d => d.Id == id);
    if (director == null)
        return Results.NotFound();
    
    context.Persons.Remove(director);
    await context.SaveChangesAsync();
    
    return Results.NoContent();
}).WithOpenApi();

app.MapPut("/api/director/{id}", async (Guid id, CreateDirector request, CatalogDbContext context) =>
{
    var director = await context.Persons.FirstOrDefaultAsync(d => d.Id == id);
    if (director == null)
        return Results.NotFound();
    
    director.FirstName = request.FirstName;
    director.LastName = request.LastName;
    
    await context.SaveChangesAsync();
    
    return Results.Ok(director);
}).WithOpenApi();

app.Run();

public record CreateGenre(string Name);
public record CreateDirector(string FirstName, string LastName);
public record CreateMovie(string Title, string EnglishTitle, string Origin, DateTime ReleasedAt, string Cover, string Trailer, string Description, Guid DirectorId, Guid GenreId, int Price);

public partial class Program {}
using System.Net;
using System.Text;
using FluentAssertions;

namespace NextFlix.Content.IntegrationsTests;

public class DirectorTests : BaseIntegrationTest
{
    public DirectorTests(IntegrationTestWebAppFactory factory) : base(factory)
    {
    }

    [Fact]
    public async Task Get_Directors()
    {
        var apiUrl = "/api/directors";
        
        var response = await Client.GetAsync(apiUrl);
        
        response.StatusCode.Should().Be(HttpStatusCode.OK);
        
        var content = await response.Content.ReadAsStringAsync();
        content.Should().NotBeNullOrWhiteSpace();
    }

    [Fact]
    public async Task Create_Director()
    {
        var apiUrl = "/api/director";
        var payload = """
                      {
                        "firstName": "John",
                        "lastName": "Doe"
                      }
                      """;
        
        var response = await Client.PostAsync(apiUrl, new StringContent(payload, Encoding.UTF8, "application/json"));
        response.StatusCode.Should().Be(HttpStatusCode.Created);
        
        var directorUrl = response.Headers.Location?.ToString()?? throw new Exception("Location header not found");
        
        var getResponse = await Client.GetAsync(directorUrl);
        getResponse.StatusCode.Should().Be(HttpStatusCode.OK);
        
        var content = await getResponse.Content.ReadAsStringAsync();
        content.Should().NotBeNullOrWhiteSpace()
            .And.Contain("John")
            .And.Contain("Doe");
    }

    [Fact]
    public async Task Delete_Director()
    {
        var apiUrl = "/api/director";
        var payload = """
                      {
                        "firstName": "John",
                        "lastName": "Doe"
                      }
                      """;
        
        var response = await Client.PostAsync(apiUrl, new StringContent(payload, Encoding.UTF8, "application/json"));
        response.StatusCode.Should().Be(HttpStatusCode.Created);

        var directorUrl = response.Headers.Location?.ToString() ?? throw new Exception("Location header not found");
        
        var deleteResponse = await Client.DeleteAsync(directorUrl);
        deleteResponse.StatusCode.Should().Be(HttpStatusCode.NoContent);
        
        var getResponse = await Client.GetAsync(directorUrl);
        getResponse.StatusCode.Should().Be(HttpStatusCode.NotFound);
    }

    [Fact]
    public async Task Update_Director()
    {
        var apiUrl = "/api/director";
        var payload = """
                      {
                        "firstName": "John",
                        "lastName": "Doe"
                      }
                      """;
        
        var response = await Client.PostAsync(apiUrl, new StringContent(payload, Encoding.UTF8, "application/json"));
        response.StatusCode.Should().Be(HttpStatusCode.Created);

        var directorUrl = response.Headers.Location?.ToString() ?? throw new Exception("Location header not found");
        
        var putPayload = """
                         {
                           "firstName": "Jane",
                           "lastName": "Doe"
                         }
                         """;
        var putResponse = await Client.PutAsync(directorUrl, new StringContent(putPayload, Encoding.UTF8, "application/json"));
        putResponse.StatusCode.Should().Be(HttpStatusCode.OK);
        
        var getResponse = await Client.GetAsync(directorUrl);
        getResponse.StatusCode.Should().Be(HttpStatusCode.OK);
        
        var content = await getResponse.Content.ReadAsStringAsync();
        content.Should().NotBeNullOrWhiteSpace()
            .And.Contain("Jane")
            .And.Contain("Doe");
    }
}
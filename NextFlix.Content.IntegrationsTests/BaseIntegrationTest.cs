using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace NextFlix.Content.IntegrationsTests;

public class BaseIntegrationTest : IClassFixture<IntegrationTestWebAppFactory>, IDisposable
{
    private readonly IServiceScope _scope;   
    protected readonly CatalogDbContext DbContext;
    protected readonly HttpClient Client;

    
    protected BaseIntegrationTest(IntegrationTestWebAppFactory factory)
    {
        _scope = factory.Services.CreateScope();
        
        Client = factory.CreateDefaultClient();
        DbContext = _scope.ServiceProvider.GetRequiredService<CatalogDbContext>();
    }
    
    public void Dispose()
    {
        _scope?.Dispose();
        DbContext?.Dispose();
    }
}
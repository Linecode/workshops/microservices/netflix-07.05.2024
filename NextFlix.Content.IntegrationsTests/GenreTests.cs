namespace NextFlix.Content.IntegrationsTests;

using System.Net;
using System.Text;
using FluentAssertions;

public class GenreTests : BaseIntegrationTest
{
    public GenreTests(IntegrationTestWebAppFactory factory) : base(factory)
    {
    }

    [Fact]
    public async Task Get_Genres()
    {
        var url = "/api/genres";
        var response = await Client.GetAsync(url);
        
        response.StatusCode.Should().Be(HttpStatusCode.OK);
        
        var content = await response.Content.ReadAsStringAsync();
        content.Should().NotBeNullOrWhiteSpace();
    }

    [Fact]
    public async Task Create_Genre()
    {
        var url = "/api/genre";
        var payload = """
                      {"name": "Random Name"}
                      """;
        var response = await Client.PostAsync(url, new StringContent(payload, Encoding.UTF8, "application/json"));
        
        response.StatusCode.Should().Be(HttpStatusCode.Created);
    }

    [Fact]
    public async Task Get_Single_Genre()
    {
        var url = "/api/genre";
        var payload = """
                      {"name": "Random Name"}
                      """;
        var response = await Client.PostAsync(url, new StringContent(payload, Encoding.UTF8, "application/json"));
        
        response.StatusCode.Should().Be(HttpStatusCode.Created);

        var genreId = response.Headers.Location?.ToString() ?? throw new Exception("Location header not found");
        
        var getResponse = await Client.GetAsync(genreId);
        
        getResponse.StatusCode.Should().Be(HttpStatusCode.OK);
        
        var content = await getResponse.Content.ReadAsStringAsync();
        content.Should().NotBeNullOrWhiteSpace()
            .And.Contain("Random Name");
    }

    [Fact]
    public async Task Delete_Genre()
    {
        var url = "/api/genre";
        var payload = """
                      {"name": "Random Name"}
                      """;
        var response = await Client.PostAsync(url, new StringContent(payload, Encoding.UTF8, "application/json"));
        
        response.StatusCode.Should().Be(HttpStatusCode.Created);

        var genreId = response.Headers.Location?.ToString() ?? throw new Exception("Location header not found");
        
        var deleteResponse = await Client.DeleteAsync(genreId);
        
        deleteResponse.StatusCode.Should().Be(HttpStatusCode.NoContent);
        
        var getResponse = await Client.GetAsync(genreId);
        
        getResponse.StatusCode.Should().Be(HttpStatusCode.NotFound);
    }

    [Fact]
    public async Task Update_Genre()
    {
        var url = "/api/genre";
        var payload = """
                      {"name": "Random Name"}
                      """;
        var response = await Client.PostAsync(url, new StringContent(payload, Encoding.UTF8, "application/json"));
        
        response.StatusCode.Should().Be(HttpStatusCode.Created);

        var genreId = response.Headers.Location?.ToString() ?? throw new Exception("Location header not found");
        
        var putPayload = """
                         {"name": "Updated Name"}
                         """;
        var putResponse = await Client.PutAsync(genreId, new StringContent(putPayload, Encoding.UTF8, "application/json"));
        
        putResponse.StatusCode.Should().Be(HttpStatusCode.OK);
        
        var getResponse = await Client.GetAsync(genreId);
        
        getResponse.StatusCode.Should().Be(HttpStatusCode.OK);
        
        var content = await getResponse.Content.ReadAsStringAsync();
        content.Should().NotBeNullOrWhiteSpace()
            .And.Contain("Updated Name");
    }
}
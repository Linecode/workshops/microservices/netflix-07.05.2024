using System.Net;
using System.Text;
using FluentAssertions;

namespace NextFlix.Content.IntegrationsTests;

public class MovieTests : BaseIntegrationTest
{
    public MovieTests(IntegrationTestWebAppFactory factory) : base(factory)
    {
    }

    [Fact]
    public async Task Get_Movies()
    {
        var apiUrl = "/api/movies";
        
        var response = await Client.GetAsync(apiUrl);
        
        var content = await response.Content.ReadAsStringAsync();
        response.StatusCode.Should().Be(HttpStatusCode.OK);
        
        content.Should().NotBeNullOrWhiteSpace();
    }

    [Fact]
    public async Task Create_Movie()
    {
        var genreId = DbContext.Genres.First().Id;
        var directorId = DbContext.Persons.First().Id;
        var apiUrl = "/api/movie";
        var payload = """
                      {
                        "title": "Random Title",
                        "englishTitle": "string",
                        "origin": "string",
                        "description": "Random Description",
                        "releasedAt": "2024-05-05T12:44:34.943Z",
                        "cover": "string",
                        "trailer": "string",
                        "directorId": "{directorId}",
                        "genreId": "{genreId}",
                        "price": 350
                      }
                      """;
        payload = payload.Replace("{genreId}", genreId.ToString()).Replace("{directorId}", directorId.ToString());
        
        var response = await Client.PostAsync(apiUrl, new StringContent(payload, Encoding.UTF8, "application/json"));
        response.StatusCode.Should().Be(HttpStatusCode.Created);
        
        var movieUrl = response.Headers.Location?.ToString()?? throw new Exception("Location header not found");
        
        var getResponse = await Client.GetAsync(movieUrl);
        getResponse.StatusCode.Should().Be(HttpStatusCode.OK);
        
        var content = await getResponse.Content.ReadAsStringAsync();
        content.Should().NotBeNullOrWhiteSpace()
            .And.Contain("Random Title");
    }

    [Fact]
    public async Task Update_Movie()
    {
        var genreId = DbContext.Genres.First().Id;
        var directorId = DbContext.Persons.First().Id;
        var apiUrl = "/api/movie";
        var payload = """
                      {
                        "title": "Random Title",
                        "englishTitle": "string",
                        "origin": "string",
                        "description": "Random Description",
                        "releasedAt": "2024-05-05T12:44:34.943Z",
                        "cover": "string",
                        "trailer": "string",
                        "directorId": "{directorId}",
                        "genreId": "{genreId}",
                        "price": 350
                      }
                      """;
        payload = payload.Replace("{genreId}", genreId.ToString()).Replace("{directorId}", directorId.ToString());
        
        var response = await Client.PostAsync(apiUrl, new StringContent(payload, Encoding.UTF8, "application/json"));
        response.StatusCode.Should().Be(HttpStatusCode.Created);
        
        var movieUrl = response.Headers.Location?.ToString()?? throw new Exception("Location header not found");
        var updatedGenreId = DbContext.Genres.OrderBy(x => x.Id).Last().Id;
        var updatedFirectorId = DbContext.Persons.OrderBy(x => x.Id).Last().Id;
        var updatePaylod = """
                           {
                             "title": "Updated Random Title",
                             "englishTitle": "updated string",
                             "origin": "updated string",
                             "description": "updated Random Description",
                             "releasedAt": "2020-05-05T12:44:34.943Z",
                             "cover": "updated string",
                             "trailer": "updated string",
                             "directorId": "{directorId}",
                             "genreId": "{genreId}",
                             "price": 350
                           }
                           """;
        updatePaylod = updatePaylod.Replace("{genreId}", updatedGenreId.ToString()).Replace("{directorId}", updatedFirectorId.ToString());
        
        var updateResponse = await Client.PutAsync(movieUrl, new StringContent(updatePaylod, Encoding.UTF8, "application/json"));
        updateResponse.StatusCode.Should().Be(HttpStatusCode.OK);
        
        var getResponse = await Client.GetAsync(movieUrl);
        getResponse.StatusCode.Should().Be(HttpStatusCode.OK);
        
        var content = await getResponse.Content.ReadAsStringAsync();
        content.Should().NotBeNullOrWhiteSpace()
            .And.Contain("Updated Random Title");
    }

    [Fact]
    public async Task Delete_Movie()
    {
        var genreId = DbContext.Genres.First().Id;
        var directorId = DbContext.Persons.First().Id;
        var apiUrl = "/api/movie";
        var payload = """
                      {
                        "title": "Random Title",
                        "englishTitle": "string",
                        "origin": "string",
                        "description": "Random Description",
                        "releasedAt": "2024-05-05T12:44:34.943Z",
                        "cover": "string",
                        "trailer": "string",
                        "directorId": "{directorId}",
                        "genreId": "{genreId}",
                        "price": 350
                      }
                      """;
        payload = payload.Replace("{genreId}", genreId.ToString()).Replace("{directorId}", directorId.ToString());
        
        var response = await Client.PostAsync(apiUrl, new StringContent(payload, Encoding.UTF8, "application/json"));
        response.StatusCode.Should().Be(HttpStatusCode.Created);
        
        var movieUrl = response.Headers.Location?.ToString()?? throw new Exception("Location header not found");
        
        var deleteResponse = await Client.DeleteAsync(movieUrl);
        deleteResponse.StatusCode.Should().Be(HttpStatusCode.NoContent);
        
        var getResponse = await Client.GetAsync(movieUrl);
        getResponse.StatusCode.Should().Be(HttpStatusCode.NotFound);
    }
}